#!/usr/bin/env bash

#############
# Functions #
#############

#############
# Variables #
#############
_nvim='FLATPAK_ENABLE_SDK_EXT=node18,golang flatpak run io.neovim.nvim'
export VISUAL="$_nvim"
export EDITOR="$_nvim"

###########
# Aliases #
###########
set_alias 'nvim' "$_nvim"
set_alias 'v' "$_nvim"
