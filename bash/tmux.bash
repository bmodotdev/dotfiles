function tmux () {
    case "$1" in
        a)
            shift
            gum_tmux_a
            ;;
        *)
            command -p tmux "$@"
            ;;
    esac
}

function gum_tmux_a () {
    # gum only
    if ! command -v gum >/dev/null 2>&1; then
        error 'Required command “gum” not installed: https://github.com/charmbracelet/gum'
        return
    fi

    local _sessions
    _sessions="$(command -p tmux list-sessions -F \#S)"
    if [[ -z "$_sessions" ]] ; then
        info 'No tmux sessions found... creating new session'
        command -p tmux
        return
    fi

    local _count _session
    _count="$(command -p wc -l <<<"$_sessions")"
    if [[ "$_count" -eq 1 ]]; then
        _session="$_sessions"
    else
        _session="$(command gum filter --placeholder 'Pick session...' <<<"$_sessions")"
    fi

    command -p tmux switch-client -t $_session \
        || command -p tmux attach -t $_session
}
