function tz() {
  declare -a _zones
  declare -i _zone_width=0

  # gum only
  if ! command -v gum >/dev/null 2>&1; then
    error 'Required command “gum” not installed'
    return
  fi

  if [ "$#" -gt 1 ]; then
    for _zone in "$@"; do
      # Dynamically set zone width
      [ "${#_zone}" -gt "$_zone_width" ] && _zone_width="${#_zone}"
      _zones+=("$_zone")
    done
  else
    while true; do

      # Print selected zones
      if [ "${#_zones[@]}" -gt 0 ]; then
        command -p clear
        gum style --foreground 212 --border-foreground 212 --border double \
          --align center --width 50 --margin "1 2" --padding "2 4" \
          "${_zones[@]}"
      fi

      # Prompt for more zones after 2 selected
      if [ "${#_zones[@]}" -ge 2 ]; then
        gum confirm 'Select another time zone?' || break
      fi

      # Select zone
      declare _zone="$(_tz_select "${_zones[*]}")"
      [ -z "$_zone" ] && break

      # Dynamically set zone width
      [ "${#_zone}" -gt "$_zone_width" ] && _zone_width="${#_zone}"

      # Append new zones
      _zones+=("$_zone")
    done
  fi

  # Nothing to compare if less than 2 time zones
  if [ "${#_zones[@]}" -lt 2 ]; then
    return
  fi

  # Append row/element
  declare -a _output=('Zone,Current,-3,-2,-1,Now,1,2,3,Zone')
  for _zone in "${_zones[@]}"; do
    declare _ranges="$(_tz_range "$_zone")"
    _output+=("$(printf '%s,%s,%s,%s' "$_zone" "$(TZ="$_zone" date '+%c')" "$_ranges" "$_zone")")
  done

  # Pass output to gum
  printf '%s\n' "${_output[@]}" | gum table --widths "${_zone_width}",35,2,2,2,3,2,2,2,35
  printf '%s\n' "${_output[@]}" | gum table --print --widths "${_zone_width}",35,2,2,2,3,2,2,2,35
}

function _tz_range() {
  declare -r _zone="$1"
  declare -a _hours=({00..23})
  declare -r -a _offset=(-3 -2 -1 0 1 2 3)
  declare -i _hour="$(TZ="$_zone" date '+%H')"

  declare -a _range
  for i in "${_offset[@]}"; do

    # Rotate through our array
    i="$((_hour + i))"
    [ "$i" -gt 23 ] && i="$((i % 23))"

    # Append hour with comma
    _range+=("${_hours[$i]}" ',')
  done

  # Remove trailing comma
  unset _range[-1]
  printf '%s' "${_range[@]}"
}

function _tz_select() {
  declare -r _placeholder="$1"
  declare -r _dir='/usr/share/zoneinfo'

  find "$_dir" \! \( -path "$_dir/posix/*" -o -path "$_dir/right/*" -o -name \*.tab -o -name \*.zi \) \
    -printf '%P\n' | gum filter --placeholder "$_placeholder"
}
