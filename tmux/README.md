# Setup
```
$ cd ~
$ ln -s git/dotfiles/tmux/tmux.conf .tmux.conf
```

# Tmux Cheat Sheet

## The tmux prefix
All tmux commands are prefixed by this character combination:

`ctrl + a`

Moving forward, this combo will be referred to as “prefix”.

## General
| combo                 | default   | description                   |
|-----------------------|-----------|-------------------------------|
| `prefix + ctrl + e`   | no        | edit and reload tmux config   |
| `prefix + ctrl + r`   | no        | redload tmux config           |

## Copy Mode
| combo                 | default   | description                       |
|-----------------------|-----------|-----------------------------------|
| `prefix + [`          | yes       | enter copy mode                   |
| `h/j/k/l`             | yes       | move cursor left/down/up/right    |
| `ctrl + u/d`          | yes       | move half page up/down            |
| `ctrl + b/f`          | yes       | move full page up/down            |
| `shift + j/k`         | yes       | scroll down/up                    |
| `space`               | yes       | start selection                   |
| `y`                   | yes       | copy selection                    |
| `esc`                 | yes       | clear selection                   |


## Windows
| combo                 | default   | description                           |
|-----------------------|-----------|---------------------------------------|
| `prefix + w`          | yes       | list current sessions with preview    |
| `prefix + c`          | yes       | create new window                     |
| `prefix + n`          | yes       | switch to next window                 |
| `prefix + p`          | yes       | switch to previous window             |
| `prefix + 2`          | yes       | switch to window #2                   |
| `prefix + ,`          | yes       | rename window                         |

## Panes
| combo                 | default   | description                   |
|-----------------------|-----------|-------------------------------|
| `preifx + ;`          | yes       | switch to last pane           |
| `prefix + \|`         | no        | split plane vertically        |
| `prefix + \`          | no        | split plane vertically        |
| `prefix + _`          | no        | split plane horizontally      |
| `prefix + - `         | no        | split plane horizontally      |
| `prefix + j`          | no        | select below pane             |
| `prefix + k`          | no        | select above pane             |
| `prefix + h`          | no        | select left pane              |
| `prefix + l`          | no        | select right pane             |
| `prefix + ctrl + j`   | no        | resize pane down              |
| `prefix + ctrl + k`   | no        | resize pane up                |
| `prefix + ctrl + h`   | no        | resize pane left              |
| `prefix + ctrl + l`   | no        | resize pane right             |
