# Enable userChrome
Open `about:config` and set the following:
```
toolkit.legacyUserProfileCustomizations.stylesheets true
```

# Locate the profile folder
Open `about:support` and copy the "Profile Directory" path.
If using flatpak, prefix the path with `~/.var/app/org.mozilla.firefox/`.

# Create the usrChrome file
```
$ mkdir ~/.var/app/org.mozilla.firefox/.mozilla/firefox/f5x542t8.default-release/chrome
$ cp userChrome.css ~/.var/app/org.mozilla.firefox/.mozilla/firefox/f5x542t8.default-release/chrome/userChrome.css
```

Restart Firefox
